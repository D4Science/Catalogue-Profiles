# Catalogue-Profiles

This repository contains the profiles characterising the item typologies defined in the D4Science Catalogues  

## Author

* **Leonardo Candela** ([ORCID](https://orcid.org/0000-0002-7279-2727)) - [ISTI-CNR Infrascience Group](http://infrascience.isti.cnr.it)